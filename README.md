**Assignment 5**

**Fun with Word Ladders** 

**EE422C Fall 2014**

----------

**Assigned:**Tuesday, Nov. 7th  

**Due:**	Program  due Monday,Nov, 17th; 
	Designs due in lab, Nov, 10th or 12th   

**Points:**	20 points.  See the Grading Details section below. 

You will be working in teams of 3 for this assignment as determined by me (see the BB for team assignments).  All team members will get the same initial score, which will then be adjusted by individual participation level. 

The aim of this assignment is to give you experience working with recursion and various JCF collections, as well as to strengthen your algorithm and ADT design skills. 

**Grading details**:

Your assignment will be graded first by compiling and testing it for correctness. After that, we will read your code to determine whether all requirements were satisfied, as well as judge the overall style of your code. Points will be deducted for poor design decisions and un-commented, or unreadable code as determined by the TA.  Here is the general point breakdown: 

     *   Quality of the design : 4 points

     *   Overall Correctness of the program : 10 points 

     *   Correct usage of recursion : 2 points

     *   Proper use of exception handling : 2 points 

     *   General Design structure and Coding style : 2 points. 


