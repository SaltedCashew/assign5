/*Student Name: Brad Gray, Kelvin Pang, Daniel Zelada
 *EID: bg22946, 
 *Lab Section: 16805
 *Assignment 5 - Fun with word ladders
*/

package assignment5;

import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

// do not change class name or interface it implements
public class WordLadderSolver implements Assignment5Interface
{
    // declare class members here.
	private Dictionary dict;
	private ArrayList<String> solutionPath;
	private Deque<String> solDeque;
	private boolean found;

    // add a constructor for this object. HINT: it would be a good idea to set up the dictionary there
	public WordLadderSolver(){
		dict = new Dictionary();
		solutionPath = new ArrayList<String>();
		solDeque = new ArrayDeque<String>();
		found = false;
	}
	
	public WordLadderSolver(String fileName) throws FileNotFoundException
	{
		dict = new Dictionary(fileName);
		solDeque = new ArrayDeque<String>();
		solutionPath = new ArrayList<String>();
	}

    // do not change signature of the method implemented from the interface
    @Override
    public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException {
    	try{
	    	found = false;
	    	solutionPath.clear();
	    	checkWordValidity(startWord,endWord);
	    	MakeLadder(startWord, endWord, 0);
	    	solutionPath.clear();
			int t = solDeque.size();
			if(t==0)
			{	
				throw new NoSuchLadderException("Word ladder does not exist between " + startWord + " and " + endWord + ".");}
			solutionPath = new ArrayList<String>(t);
			for( int i = 0; i < t; i++){
				solutionPath.add(solDeque.removeLast()); //solDeque contains ladder in reverse order
			}
    	}
		catch (IllegalSearchException e)
    	{ 
			solutionPath.clear();
			solutionPath.add(e.getMessage());	
		}
    	return solutionPath;
    }

    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder) {
   	 return true; //method not yet implemented
    }

    //recursive function to build the word ladder using a DFS method
    //when finished, solDeque contains the word ladder in reverse order
    private void MakeLadder(String startWord, String endWord, int pos) throws NoSuchLadderException
    {

   	 solutionPath.add(startWord);
   	 solDeque.addFirst(startWord);
   	 if(startWord.equals(endWord)){ 
   		 found = true;
   		 return;
   	 }
   	 if ( dict.hammingDistance(startWord, endWord)==1) //if words are only one letter apart, add the word and return.
   	 {
   		 solutionPath.add(endWord);
   		 solDeque.addFirst(endWord);
   		 found = true;
   		 return;
   	} 
   	ArrayList<String> temp = dict.getWords(startWord);
	if(temp == null || temp.size() == 0){ throw new NoSuchLadderException("Word Ladder does not exist between " + solDeque.removeFirst() + " and " + endWord);	}
	List<String> sortedTemp = sortByDistance(temp, endWord);

	
	for(String s : sortedTemp)
	{
		if(!found)
		{
			int changePos = dict.compareWords(startWord, s);
			if (changePos != pos && !solutionPath.contains(s)) {MakeLadder(s,endWord, changePos);}
		}
		else{break;	}
	}
	
	if(!found){solDeque.removeFirst();}

    
   }

    //given list of strings arl and a string word, this method sorts arl by hamming distance from word
    //recursive merge sort used
    //list is sorted from least number of character distance to word parameter to largest number of character difference
    private List<String> sortByDistance(List<String> arl, String word) 
    {
		if(arl.size()<2){return arl;}
    	if(arl.size() == 2){
    		int diff1 = dict.hammingDistance(arl.get(0), word);
    		int diff2 = dict.hammingDistance(arl.get(1), word);
    		if(diff1 < diff2)
    		{
    			return arl;
    		}
    		else
    		{
    			List<String> newArl = new ArrayList<String>(arl.size());
    			newArl.add(arl.get(1));
    			newArl.add(arl.get(0));
    			return newArl;
    		}
    	}
    	//split list into two for recursive sort call
    	List<String> newArl1 = arl.subList(0, arl.size()/2 + 1); 
    	List<String> newArl2 = arl.subList(arl.size() - (arl.size()/2), arl.size());
    	sortByDistance(newArl1, word);
    	sortByDistance(newArl2, word);
    	
    	//start merging smaller arrays back together
    	int k = 0; //index counter for newArl1
    	int j = 0; //index counter for newArl2
    	List<String> finalArl = new ArrayList<String>(newArl1.size()+ newArl2.size());
	    while(k < newArl1.size() && j < newArl2.size())
	    {
	    	int diff1 = dict.hammingDistance(newArl1.get(k), word);
    		int diff2 = dict.hammingDistance(newArl2.get(j), word);
    		if(diff1 < diff2)
    		{
    			finalArl.add(newArl1.get(k));
    			k++;
    		}
    		else
    		{
    			finalArl.add(newArl2.get(j));
    			j++;
    		}
	    }
	    
	    //at this point, either k = newArl1.size() or j = newArl2.size()
	    //figure out which list has values not merged and add them into the finalArl
	    if(k < newArl1.size()) 
	    {
	    	for(int k2 = k; k2 < newArl1.size(); k2 ++){finalArl.add(newArl1.get(k2));}
	    }
	    else if(j < newArl2.size())
	    {
	    	for(int j2 = j; j2 < newArl2.size(); j2 ++){finalArl.add(newArl2.get(j2));}
	    }
	    
		return finalArl;
	}

    /*
     * Used to check words for valid use in word ladder.
     * Ensures word length of 5, words are in the supplied dictionary, and words are not the same.
     * @param s1 String for comparison
     * @param s2 String for comparison
     * @throws IllegalSearchException
     * @throws NoSuchLadderException
     */
	private void checkWordValidity(String s1, String s2) throws IllegalSearchException, NoSuchLadderException
    {	
    	final int WORD_LENGTH = 5;
    	if(s2.length() != WORD_LENGTH || s1.length() != WORD_LENGTH){throw new IllegalSearchException("Incorrect Word Length!");   	}
    	if(!dict.contains(s1)){throw new IllegalSearchException("Dictionary does not contain " + s1 + ".");}
    	if(!dict.contains(s2)){throw new IllegalSearchException("Dictionary does not contain " + s2 + ".");}
    	if(s1.equals(s2)){throw new IllegalSearchException("Invalid search attempted between " + s1 + " and " + s2);}
    	
    }
}
