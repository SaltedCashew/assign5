/*Student Name: Brad Gray, Kelvin Pang, Daniel Zelada
 *EID: bg22946, 
 *Lab Section: 16805
 *Assignment 5 - Fun with word ladders
*/

package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

/**
 * Main Driver for Assignment 5
 * Fun with Word Ladders.
 * @author Brad Gray, EE422C
 * @version 1
 **/
public class A5Driver
{
	private static final boolean TEST = true; //used for programmer testing

	public static void main(String[] args)
	{
		final int NUM_ARGS = 2;
		final int DICTIONARY_INPUT = 0; //per TA on PIAZZA, input order of args is dictionary, then test words
		final int TEST_WORD_INPUT = 1;
		StopWatch sW = new StopWatch();
		sW.start();
		if (args.length != NUM_ARGS) 
		{
			System.err.println ("Error: Incorrect number of command line arguments");
			System.exit(-1);
		}
		
		 // Create a word ladder solver object - from provided template
        Assignment5Interface wordLadderSolver = null;
		Scanner inWords = null;
		try
		{	
			wordLadderSolver = new WordLadderSolver(args[DICTIONARY_INPUT]);
			inWords = new Scanner(new File(args[TEST_WORD_INPUT]));
		}	
		catch (FileNotFoundException e)
		{
			System.out.println("File Not Found!");
			e.printStackTrace();
			System.exit(-1);
		}
	    // for each line in the file call compute ladder
		String startWord, endWord;
		List<String> result;
		boolean correct;
		if(!TEST)
		{	
			if(!inWords.hasNextLine()){System.out.println("No inputs given!");}
			while(inWords.hasNextLine())
			{
				try
				{
					startWord = inWords.next();
					endWord = inWords.next();
					result = wordLadderSolver.computeLadder(startWord, endWord);
					correct = wordLadderSolver.validateResult(startWord, endWord, result); 
					if(correct == false){ System.out.println("Something's Wrong Here!"); }
					else
					{
						for (String s: result)
						{
							System.out.println(s);
						}
						System.out.println("**********\n");
					}
				}
				catch (NoSuchLadderException e) 
				{
					System.out.println(e.getMessage());	
					System.out.println("**********\n");	
				}
			}
		}		
		if(TEST) //for testing, as it says. Does not read from input file, but allows for testing of one word ladder path
		{
			try
			{
				System.out.println("Test Mode Activated!\n");
				result = wordLadderSolver.computeLadder("stone", "money");
				int i = 1;
				for(String s : result)
				{
					System.out.println(i + " " + s);
					i++;
				}
				System.out.println("**********\n");
			}
			catch (NoSuchLadderException e) 
			{
				System.out.println(e.getMessage());	
				System.out.println("**********\n");	
			}
		}

		inWords.close();
		sW.stop();
		long mytime = sW.getElapsedTime();
		System.out.println("Time elapsed in nanoseconds is: "+ mytime);
		System.out.println("Time elapsed in seconds is: "+ mytime/ StopWatch.NANOS_PER_SEC);	
	}

}
