/*Student Name: Brad Gray, Kelvin Pang, Daniel Zelada
 *EID: bg22946, 
 *Lab Section: 16805
 *Assignment 5 - Fun with word ladders
*/

package assignment5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


/**
 * Intended Storing, manipulating, cloning dictionary.
 * @author Brad Gray, EE422C
 * @version 1
 **/
public class Dictionary
{
	//can possibly combine these later on 
	private ArrayList<String> storedDictionary; 
	private HashMap<String, ArrayList<String>> ladderMap; 
	
	/**
	 * Default Constructor
	 * defines new, empty dictionary
	 **/
	public Dictionary()
	{
		storedDictionary = new ArrayList<String>();
	}
	
	/**
	 * Constructor with file name parameter.
	 * Defines new, empty dictionary.
	 * Fills dictionary from file using passed fileName.
	 * Lines starting with '*' in dictionary are ignored. Assumes 5 letters per word, one word per line.
	 * @throws FileNotFoundException if file name not found while building dictionary
	 * @param fileName String name of the dictionary file
	 **/
	public Dictionary(String fileName) throws FileNotFoundException
	{
		
		storedDictionary = new ArrayList<String>();
		try
		{
			buildDictionary(fileName);
			hashDictionary();
			
		}
		catch (FileNotFoundException e)
		{
			System.out.println("File Not Found!");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Passed parameter s and returns array list of words with one letter difference from string s.
	 * Returns null if s not found
	 * @param s as the hash key
	 * @return ArrayList of words with one letter difference from parameter string s
	 **/
	public ArrayList<String> getWords(String s)
	{
		return ladderMap.get(s);
	}
	
	/**Return the Hamming distance between equal-length strings
	 * returns -1 if strings differ in length
	 * @param s1 first String 
	 * @param s2 second string
	 * @return number of differences in the strings as an integer, or -1 for strings of different legths
	 */
	public int hammingDistance(String s1, String s2)
	{		  
		    if (s1.length() != s2.length())
		    {
		    	System.out.println("Strings not of equal length");
		    	return -1;
		    }
		    int a = 0;
		    for (int i = 0; i < s1.length(); i++) 
		    {
		        if (s1.charAt(i) != s2.charAt(i)) {    a++;       }
		    }    
		    return a;
	}
	
	/**
	 * Returns the position of the first changed letter between strings s1 and s2.
	 * Returns -1 if strings are unequal in length or if no difference found
	 * @param s1 first string for comparison
	 * @param s2 second string for comparison
	 * @return position in string as an integer or -1
	 */
	public int compareWords(String s1, String s2)
	{
		 if (s1.length() != s2.length())
	    {
	    	System.out.println("Strings not of equal length");
	    	return -1;
	    }
	    for (int i = 0; i < s1.length(); i++) 
	    {
	        if (s1.charAt(i) != s2.charAt(i)) {    return i+1;       }
	    }    
	    return -1;
	}
	
	/**
	 * Returns true if dictionary contains string s
	 * Returns false otherwise
	 * @param s String to check for in dictionary
	 * @return boolean
	 */
	public boolean contains(String s)
	{
		return(storedDictionary.contains(s));
	}
	
	/*
	 * Sets up the HashMap for the dictionary.
	 * For each word in the storedDictionary, all words on letter off stored in arraylist.
	 * HashMap <key, value>, where key is the word from the storedDictionary and value is arraylist of one-off words
	 * HashMap stored with Dictionary instance and initialized to size of stored dictionary
	 */
	private void hashDictionary()
	{
		ladderMap = new HashMap<String, ArrayList<String>>(storedDictionary.size());
		for(String s : storedDictionary)
		{
			ladderMap.put(s, getBranchWords(s));
		}
	}
	
	private ArrayList<String> getBranchWords(String s)
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(String t : storedDictionary)
		{
			if(hammingDistance(s,t)==1)
			{
				temp.add(t);
			}
		}
		
		return temp;
	}
	
	// Fills dictionary from file using passed fileName.
	// Lines starting with '*' in dictionary are ignored. Assumes 5 letters per word, one word per line.
	private void buildDictionary(String fileName) throws FileNotFoundException
	{
		Scanner inDict = null;
		try
		{
			inDict = new Scanner(new File(fileName));
			while(inDict.hasNextLine())
			{
				String temp = inDict.nextLine();
				if(temp.charAt(0) != '*')
				{
					String workingString = new String(temp.substring(0,5));
					storedDictionary.add(workingString);
				}
			}
		}
		finally
		{
			inDict.close();	
		}
		
		
	}
}
