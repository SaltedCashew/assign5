/*Student Name: Brad Gray, Kelvin Pang, Daniel Zelada
 *EID: bg22946, 
 *Lab Section: 16805
 *Assignment 5 - Fun with word ladders
*/

package assignment5;

import java.util.InputMismatchException;

/**
 * Custom exception for the Word Ladder program.
 * Allows for exception for invalid input
 * @author Brad Gray, EE422C
 * @version 1
 */
public class IllegalSearchException extends InputMismatchException
{


	private static final long serialVersionUID = 1L;
	
	/**
	 * Basic constructor with no parameter.
	 * Calls InputMismatchException constructor message "Illegal Search Attempted!"
	 */
	public IllegalSearchException()
	{
		super("Illegal Search Attempted!");
	}

	/**
	 * Constructor with string parameter.
	 * Prints passed string parameter and stack trace.
	 * @param arg0 argument string
	 */
	public IllegalSearchException(String arg0)
	{
		super(arg0);
	}

}
