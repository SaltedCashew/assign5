/*Student Name: Brad Gray, Kelvin Pang, Daniel Zelada
 *EID: bg22946, 
 *Lab Section: 16805
 *Assignment 5 - Fun with word ladders
 *Custom Exception taken from assignment 5 template
*/

package assignment5;

public class NoSuchLadderException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NoSuchLadderException(String message)
    {
        super(message);
    }

    public NoSuchLadderException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
